// var player=require('player')

// player("build/msg-1-1.mp3").play(function(err,player){
//     if (err) throw err; 
// })

// var song=new Audio();
// song.src="../build/msg-1-1.mp3"
// song.play()
var LUNA_FUNCTION_LIST= [
    "AEB"      ,
    "BSD"      ,
    "LDW"      ,
    "LCA"      ,
    "SDO"      ,
    "AHBC"     ,
    "FCTA"     ,
    "RCTA"     ,
    "FCTAB"    ,
    "APA"      ,
    "SAPA"     ,
    "ACC"      ,
    "TSR"      ,
    "ISA"      ,
    "LKA"      ,
    "ALC"      ,
    "Pilot-Lat"    ,
    "Pilot-Lon"    ,
    "Other"
];
console.log(LUNA_FUNCTION_LIST.join(","))