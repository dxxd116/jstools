var FileList =require("./config/fileList.js");
// var OutputFiles=require("./config/outputFiles.js");
var fs=require('fs')

// console.log(FileList)

FileList.forEach((filePath,index) =>{
    console.log(index, filePath)
    var outputFilePath=filePath.replace(".txt",".png")
    console.log(outputFilePath)
    fs.readFile(filePath,(err,data)=>{
        if (err) throw err;
        saveDataToFile(data.toString('ascii'), outputFilePath)
    })

})

function saveDataToFile(data,outputFilePath){
    console.log("Writting to ", outputFilePath)
    var buf=Buffer.from(data,'base64')

    fs.open(outputFilePath, 'w',(err,fd) =>{
        if (err) throw err;
        fs.write(fd,buf,(err,written,buffer)=>{
            console.log(`${written} bytes written!`)
        })
    })
}