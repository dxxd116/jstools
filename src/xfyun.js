var axios=require('axios')
var moment =require('moment')
var md5=require('md5')
var urlencode = require('urlencode');
var fs=require('fs')

var apiEndpoint='https://api.xfyun.cn/v1/service/v1/tts'
var data={
    "auf": "audio/L16;rate=16000",
    "aue": "lame",
    "voice_name": "aisjiuxu",
    "speed": "50",
    "volume": "50",
    "pitch": "50",
    "engine_type": "intp65",
    "text_type": "text"
}
var dataStr=JSON.stringify(data)
// console.log(dataStr.length)
var buf=Buffer.from(dataStr)
var xParam=buf.toString('base64')
// console.log(xParam)

var appId='5c1b55a3';
var apiKey="34fd37e419f27b205b9b1b6b9f7d1a1a"; 
var curTime=moment().unix();
var checkSum=md5(apiKey+curTime+xParam);
// console.log(checkSum)

//var postData="text=" + urlencode("你好，蔡兴亮")
var text="关闭语音"
var outputFile="build/msg-close.mp3"

// textToVoice(text,outputFile)


function textToVoice(text, outputFile){
var postData="text=" + urlencode(text)
axios.post(apiEndpoint,postData,{
    "headers": {
        'Content-Type':'application/x-www-form-urlencoded; charset=utf-8',
        'X-Appid': appId,
        'X-CurTime': curTime,
        'X-Param': xParam,
        'X-CheckSum': checkSum
    }, 
    "responseType":  'arraybuffer'
}).then((res) =>{
    // debugger
    console.log(res.headers['content-type'])
    if (res.headers['content-type']=='text/plain'){
        console.log(res.data)
    }
    if (res.headers['content-type']=="audio/mpeg"){
        fs.writeFile(outputFile, res.data, function(error){
            if (error) throw error;
            console.log("File written successfully!")
        })
    }else{

    }
}).catch((err) =>{
    
    console.log("Fail to get the response ",err)
})

}

exports.textToVoice=textToVoice